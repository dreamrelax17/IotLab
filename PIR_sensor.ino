
int PIR_sensor = 2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(PIR_sensor,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int val = digitalRead(PIR_sensor);
  Serial.println(val);
  delay(500);
}
