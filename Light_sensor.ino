
int Light_sensor = A0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(Light_sensor,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int val = analogRead(Light_sensor);
  Serial.println(val);
  delay(500);
}
